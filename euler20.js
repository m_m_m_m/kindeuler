module.exports = (function (){
  const inst_bool = x=>x(true)(false);
  const elim_bool = (x=>{var $2 = (()=>c0=>c1=>{var self = x;if (self) {var $0 = c0;return $0;} else {var $1 = c1;return $1;};})();return $2;});
  const inst_nat = x=>x(0n)(x0=>1n+x0);
  const elim_nat = (x=>{var $6 = (()=>c0=>c1=>{var self = x;if (self===0n) {var $3 = c0;return $3;} else {var $4=(self-1n);var $5 = c1($4);return $5;};})();return $6;});
  const Bool$true = true;
  const Bool$false = false;
  function Nat$is_zero$(_n$1){var self = _n$1;if (self===0n) {var $8 = Bool$true;var $7 = $8;} else {var $9=(self-1n);var $10 = Bool$false;var $7 = $10;};return $7;};
  const Nat$is_zero = x0=>Nat$is_zero$(x0);
  function Nat$succ$(_pred$1){var $11 = 1n+_pred$1;return $11;};
  const Nat$succ = x0=>Nat$succ$(x0);
  const Nat$add = a0=>a1=>(a0+a1);
  const Nat$mul = a0=>a1=>(a0*a1);
  const Nat$sub = a0=>a1=>(a0-a1<=0n?0n:a0-a1);
  function Main$fac$(_n$1){var self = Nat$is_zero$(_n$1);if (self) {var $13 = 1n;var $12 = $13;} else {var $14 = (_n$1*Main$fac$((_n$1-1n<=0n?0n:_n$1-1n)));var $12 = $14;};return $12;};
  const Main$fac = x0=>Main$fac$(x0);
  return {
    'Bool.true': Bool$true,
    'Bool.false': Bool$false,
    'Nat.is_zero': Nat$is_zero,
    'Nat.succ': Nat$succ,
    'Nat.add': Nat$add,
    'Nat.mul': Nat$mul,
    'Nat.sub': Nat$sub,
    'Main.fac': Main$fac,
  };
})();
var MAIN=module.exports['Main.fac']; try { console.log(JSON.stringify(MAIN,null,2) || '<unprintable>') } catch (e) { console.log(MAIN); };
