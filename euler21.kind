// Solution for Euler 21 problem: https://projecteuler.net/problem=21

Main: IO(Unit)
  IO {
    divisors_sum = Main.map_of_divisors_sum
    sum_amicable = Main.sum_of_amicable_numbers(divisors_sum)
    sum_amicable_str = Nat.to_string_base(10, sum_amicable)
    IO.print(sum_amicable_str)
  }  

Main.map_of_divisors_sum: Map(Nat)
    map = {}
    for elem from 2 to 10000 with map:
        divisors_sum = Main.sum_of_divisors(elem)
        map = Map.set!(Nat.to_string_base(10, elem), divisors_sum, map)
        map
    map


Main.sum_of_amicable_numbers(m: Map(Nat)): Nat
    keys = Map.keys<Nat>(m)
    let sum = 0
    for k in keys with sum:
        let k_nat = Nat.read(k)
        let divisors_sum = m{k}
        sum = case divisors_sum {
            some:
                let divisors_sum_value = divisors_sum.value
                let divisors_sum_string = Lib.ns(divisors_sum_value)
                let m_amicable_number = m{divisors_sum_string}
                amicable_number = case m_amicable_number {
                    some:
                        let amicable_number_value = m_amicable_number.value
                        if Bool.and(amicable_number_value =? k_nat,  Bool.not(k_nat =? divisors_sum_value))
                            then 
                                log("÷Amicable number of " | k | "( "| divisors_sum_string | " )" | " is " | Lib.ns(amicable_number_value))
                                divisors_sum_value
                            else 0
                    none:
                        0
                }
                sum + amicable_number
            none:
                sum
        }
        sum
    sum


Main.sum_of_divisors(n:Nat): Nat
    divisors = Main.divisors(n, 2, [])
    List.fold!(divisors, _, 1, (x, xs) x + xs)

Main.divisors (n: Nat, nn: Nat, d: List<Nat>): List<Nat>
    if Nat.gtn(nn, Nat.add(Nat.div(n, 2), 1))
        then d
        else 
            mod = Nat.mod(n, nn)
            if Nat.eql(mod, 0)
                then Main.divisors(n, nn + 1, List.append!(d, nn))
                else Main.divisors(n, nn + 1, d)
            