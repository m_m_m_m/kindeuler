const chokidar = require('chokidar');
const chalk = require('chalk');
const { exec } = require("child_process");

const log = console.log;

const watcher = chokidar.watch('../', {ignored: /\.(?!(kind)$)([^.]+$)/, persistent: true});

const compile = (/** @type String */ path, /** @type Boolean **/ quiet = true) => {
    log(chalk.blue("Compilling..."))
    exec(`kind ${path}`, (error, stdout, stderr) => {
            if (error) {
                log(chalk.red("There was an internal error during compillation of ", path))
                log(chalk.bgRed(error))
            }
            if (stdout) {
                if (stdout.includes("With context") || stdout.includes("Undefined reference")) {
                    log(chalk.bgGrey(chalk.red(stdout)))
                } else {
                    log("File ", chalk.blue(path), " has been compilled with success!")
                    if (quiet == false) {
                        log(chalk.bgBlue(chalk.black(stdout)))
                     }
                }
            }
            if (stderr) {
                log(stderr)
            }
        })
}


watcher
    .on('add', function (path) {
        log("I am watching ", chalk.blue(path));
        compile(path)
    })
    .on('change', function (path) {
        log('File has been changed', chalk.blue(path));
        compile(path, false)
    })
    .on('unlink', function (path) {
        log('File', path, 'has been removed');
    })
    .on('error', function (error) {
        console.error('Error happened', error);
    })